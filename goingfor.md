For features and other wishes please create an issue! thx

== What is to be changed ==

types of targets (tt#)
* 1 Features, still need to be identified (Jotunbane has a list, Ewa might collect some of prior proposals)
* 2 Coding Paradigm, reorganize structure, separate data, logic etc. to be more flexible over time
* 3 Bugfixes or prevent potential bugs

=== Changes ===

this is happing now on a dev branch and may be approved by Jotunbane

* top location href after form interaction (tt1)
  - set ancors
  - add ancor to location, maybe change location
* send header just once (tt3)
  - make use of header_sent() to check beforehand
  - prepare string instead of calling functions everywhere
* remove global declared variables (tt2)
  - **this may cause disruption** while going for
  - reasoning: if we want to enable object oriented programming encapsulation is a target, so we need to stick to scopes
  - variables for now are added to parameters of the function
* edit escaped characters (tt2)
  - The quotes are taken care of so that less escaping is needed
  - variables are separated from string so you can spot them easier in the code
  - together this can be preparation for templates and language-switching if needed in future
* print $would when born is known only (tt3)
* remaking sizeFormat a bit more flexible (tt3)
  - no copyright any more
  - input exponent can be changed
  - base can be changed
  - set of units can be chosen
* (re)implementing  del_dir
  - no copyright any more
  - same params
  - same states
  - still shorter
  - no newer php version needed
  - no handle needed
  - parameter for path-delimiter
  - suggestions added for future features
* (re)implementing getDirectorySize
  - same params and return
  - not showing warnings
  - taking care of other filetypes as well
* moving date_default_timezone_set to central function
  - change can be made easily

== ToDo identified ==

* db functions in class
* get all header manipulation to single place
* msg function in class
* deleteelement() : deal with pictures
* denydocument() : implement
* confirmdocument() : check validity of request
* getnextid() : use the SQL keyword serial
* search for copyrighted parts and reimplement things for use under terms of AGPL
* "Content" like messages to the world should not be in source