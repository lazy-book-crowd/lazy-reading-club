<?php

/* reimplementing, target license is AGPL
 * 
 * used to show file size with unit
 * 
 * n - number, feasible is (platform dependant) PHP_INT_SIZE
 * s - space character, e.g. for non breaking html space
 * b - base for correct numbers and unit names
 * u - units as array[base][exponent]
 * e - calculated exponent
 * i - index/exponent of biggest unit
 * 
 * IEC prefix, properly explained:
 *   https://en.wikipedia.org/wiki/Binary_prefix
 * remember to use
 *   + decimal -  is data transfer (base 1000: in KB, MB, GB, ...)
 *   + binary -  is data storage (base 1024: in KiB, MiB, GiB, ... )
 */
function sizeFormat( $n, $s='&nbsp;', $b=1024, $u = array(
        // YiB is 2^80, x64 is PHP_INT_SIZE
        // => so 2^60 is max feasible for now
        '1024' => array("bytes", "KiB", "MiB", "GiB",
            "TiB", "PiB", "EiB"/*, "ZiB", "YiB"/**/ ),
        '1000' => array("bytes", "KB", "MB", "GB",
            "TB", "PB", "EB"/*, "ZB", "YB"/**/ ),
    ) ) {
    $e  = (int)log( $n, $b ) ;
    $n /= ( ( $b == 0 && $e != 0 ) ? pow( $b, $e ) : 1 );
    $i  = sizeof( $u[$b] )-1;
    if( $e > $i ){
        $n *= pow( $b, $e-$i );
        $e -= $e-$i;
    }
    return round( $n, 2 ) . $s . $u[$b][$e];
}

/* reimplementing, target license is AGPL
 *
 * dirname - directory to delete recursively
 * 
 * suppressing warning for errno 13: 'Permission denied'
 * no extended validation for dirname herein yet
 * possible addition: array of files that shall not be deleted
*/
function del_dir( $dirname, $delim='/' ) {
    if( !is_dir($dirname) ) {
        //return unlink($dirname); //in case that is what you want
        return false;
    }
    if( false === ( $dirlist = @scandir( $path ) ) ) {
        return false;
    }
    foreach( array_diff( $dirlist, array( '.', '..' ) ) as $file ) {
        if( is_dir( $dirname . $delim . $file ) ) {
            del_dir( $dirname . $delim . $file );
        } else {
            unlink( $dirname . $delim . $file );
        }
    }
    return del_dir( $dirname );
}

/* reimplementing, target license is AGPL
 * 
 * suppressing warning for errno 13: 'Permission denied'
 * suppressing warning for filetype: 'Lstat failed'
 * counting filetype 'dir' and 'file' only
 */
function getDirectorySize( $path, $delim='/' ) {
    $r = array('size' => 0, 'count' => 0, 'dircount' => 0);
    if( !file_exists( $path ) 
        || false === ( $dirlist = @scandir( $path ) )
    ) {
        return false;
    }
    foreach( array_diff( $dirlist, array('.', '..') ) as $file ) {
        $nextpath = $path . $delim . $file;
        switch( @filetype( $nextpath ) ) {
            case 'dir':
                $result = getDirectorySize( $nextpath );
                $r['size'] += $result['size'];
                $r['count'] += $result['count'];
                $r['dircount'] += $result['dircount']+1;
                break;
            case 'file':
                $r['size'] += filesize( $nextpath );
                $r['count']++;
                break;
            default:
            /* not counting in other types here, others known are:
             *     'link', 'char', 'block', 'socket', 'fifo', ''
             */
        }
    }
    return $r;
}